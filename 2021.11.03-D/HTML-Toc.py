# HTML-Toc.py
import ply.lex as plex


class HTMLLex:
    tokens = ("TEXT", "HOPEN", "HCLOSE", "OTAG")

    def t_TEXT(self, t):
        r"[^<]+"
        if self.inside_header:
            print(t.value)
        return t

    def t_HOPEN(self, t):
        r"<[hH][1-6][^>]*>"
        self.inside_header = True
        return t

    def t_HCLOSE(self, t):
        r"</[hH][1-6]>"
        self.inside_header = False
        return t

    def t_OTAG(self, t):
        r"<[^>]+>"
        return t

    def t_error(self, t):
        print(f"Unexpected tokens: {t.value[:10]}")
        exit(1)

    def __init__(self, filename):
        self.lexer = None
        self.filename = filename
        self.inside_header = False

    def toc(self, **kwargs):
        self.lexer = plex.lex(module=self, **kwargs)
        with open(self.filename, "r") as fh:
            contents = fh.read()
        self.lexer.input(contents)

        for token in iter(self.lexer.token, None):
            pass
        print("Finished processing")


processor = HTMLLex("ply.html")
processor.toc()
