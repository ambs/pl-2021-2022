# calculator.py

from CalcLexer import CalcLexer

calc = CalcLexer()   # __init__
calc.initialize()    # inicializar o lexer

for line in iter(lambda: input(">> "), ""):
    result = calc.calc(line)
    print(f"={result}")




