# CalcLexer.py
import ply.lex as plex
from stack_calculator import StackCalculator


class CalcLexer:
    t_ignore = " "
    tokens = ("ADD", "SUB", "DIV", "MUL", "NUMBER")
    t_ADD = r"ADD"
    t_SUB = r"SUB"
    t_DIV = r"DIV"
    t_MUL = r"MUL"

    def t_NUMBER(self, t):
        r"[0-9]+(\.[0-9]+)?"
        t.value = float(t.value)
        return t

    def t_error(self, t):
        print(f"Unexpected tokens: {t.value[:10]}")
        exit(1)

    def __init__(self):
        self.lexer = None

    def initialize(self, **kwargs):
        self.lexer = plex.lex(module=self, **kwargs)

    def calc(self, formula):
        self.lexer.input(formula)
        stack = StackCalculator()
        for token in iter(self.lexer.token, None):
            if token.type == "NUMBER":
                stack.push(token.value)
            elif token.type == "DIV":
                stack.div()
            elif token.type == "MUL":
                stack.mul()
            elif token.type == "ADD":
                stack.add()
            elif token.type == "SUB":
                stack.sub()
        return stack.top()
