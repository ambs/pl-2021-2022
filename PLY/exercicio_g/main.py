#!/usr/bin/env python3
# author: Rui Alves @ IPCA 15505

import ply.lex as plex
from my_lib import slurp
from calculator import Calculator

calc = Calculator()

tokens = ("OP", "SPACE", "NUMBERS")


def t_SPACE(t):
    r"""[ ]+"""
    pass


def t_OP(t):
    r"""ADD|MUL|SUB|DIV"""
    calc.calculate(t.value)
    pass


def t_NUMBERS(t):
    r"""\-?[0-9]+(\.[0-9]+)?"""
    calc.push(float(t.value))
    pass


def t_error(t):
    print(f"Unexpected token error:{t.value[:10]}")
    exit(1)


lexer = plex.lex()
lexer.input(slurp("math_table.txt"))
lexer.token()
print(calc.top())
