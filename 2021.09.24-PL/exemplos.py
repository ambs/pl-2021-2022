# import os
from os import getcwd

print(getcwd())

# for (a = 0; a < 10; a+=2)

for a in range(0, 10):  # teste
    print(a)

meses = ["jan", "fev", "mar", "abr", "mai"]
for a in range(0, len(meses), 2):
    print(meses[a])


def fact(n):
    #  return n > 1 ? n * factorial(n-1) : 1;
    # if n > 1:
    #    return n * factorial(n-1)
    # else:
    #    return 1
    return n * fact(n-1) if n > 1 else 1


lista = [fact(i) for i in range(0, 10) if i % 2 == 0]
print(lista)


def count_chars(string, char=" "):
    return len([s for s in range(0, len(string))
                if string[s] == char])


frase = "Eu vi um sapo, um grande sapo!"
print(f"Nr espaços: {count_chars(frase)}")
print(f"""Nr de 'a's: {count_chars(frase, "a")}""")


class Rect:

    def __init__(self, width=10, height=20):
        self.width = width
        self.height = height
        self._area = None

    def __repr__(self):
        return f"Rect({self.width},{self.height})"

    def __str__(self):
        return "[%.2f, %.2f]" % (self.width, self.height)

    def area(self):
        if self._area is None:
            self._area = self.width * self.height
        return self._area


rect = Rect(20, 30)
print(repr(rect))
print(rect.area())
