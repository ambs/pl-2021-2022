# logic.py
from logic_grammar import LogicGrammar

lg = LogicGrammar()
for e in iter(lambda: input(">> "), ""):
    try:
        print(f"<< {lg.parse(e)}", end="\n\n")
    except Exception as exception:
        print(exception)


