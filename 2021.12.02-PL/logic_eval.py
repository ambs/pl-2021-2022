# logic_eval


class LogicEval:
    # Dispatch Table (Design Pattern)
    operators = {
        "or": lambda args: args[0] or args[1],
        "and": lambda args: args[0] and args[1],
        "xor": lambda a: a[0] ^ a[1],
        "not": lambda a: not a[0]
    }

    @staticmethod
    def eval(ast):
        if type(ast) is bool:
            return ast
        if type(ast) is dict:
            return LogicEval._eval_dict(ast)
        raise Exception(f"Eval called with weird type: {type(ast)}")

    @staticmethod
    def _eval_dict(ast):
        op = ast["op"]
        # args = [LogicEval.eval(x) for x in ast["args"]]
        args = list(map(LogicEval.eval, ast["args"]))

        if op in LogicEval.operators:
            func = LogicEval.operators[op]
            return func(args)
        else:
            raise Exception(f"Unknown operator: {op}")
