# logic_grammar.py
from logic_lexer import LogicLexer
from logic_eval import LogicEval
import ply.yacc as pyacc


class LogicGrammar:
    precedence = (
        ("left", "or", "xor"),   # menor prioridade
        ("left", "and"),
    )

    def p_error(self, p):
        if p:
            raise Exception(f"Parse Error: Unexpected token '{p.type}'")
        else:
            raise Exception("Parse Error: Expecting token")

    def p_s1(self, p):
        """ s : b """
        p[0] = p[1]

    def p_s2(self, p):
        """ s : var assign b """
        # FIXME

    def p_b1(self, p):
        """ b : f
              | b or b
              | b and b
              | b xor b """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = dict(op=p[2], args=[p[1], p[3]])

    def p_f0(self, p):
        """ f : var """
        # FIXME

    def p_f1(self, p):
        """ f : true """
        p[0] = True

    def p_f2(self, p):
        """ f : false """
        p[0] = False

    def p_f3(self, p):
        """ f : not f """
        p[0] = dict(op="not", args=[p[2]])

    def p_f4(self, p):
        """ f : '(' b ')' """
        #   ^    ^  ^  ^
        #   0    1  2  3
        p[0] = p[2]

    def __init__(self):
        self.lexer = LogicLexer()
        self.tokens = self.lexer.tokens
        self.yacc = pyacc.yacc(module=self)

    def parse(self, expression):
        ans = self.yacc.parse(lexer=self.lexer.lex, input=expression)
        return LogicEval.eval(ans)




