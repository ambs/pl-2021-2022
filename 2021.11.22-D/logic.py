# logic.py

import ply.lex as plex

class Lexer:
    tokens = ("not", "and", "or", "true", "false")  #, "(", ")", "$")
    literals = ['(', ')', '$']
    t_ignore = " "

    def __init__(self):
        self.lexer = None

    def build(self, string, **kwargs):
        self.lexer = plex.lex(module=self, **kwargs)
        self.lexer.input(string)

    def t_str(self, t):
        r"not|true|false|and|or"
        t.type = t.value
        return t

    def token(self):
        token = self.lexer.token()
        return token if token is None else token.type

    def t_error(self, t):
        print(f"Unexpected token: [{t.value[:10]}]")
        exit(1)


class LogicGrammar:
    
    def __init__(self, expr):
        self.lexer = Lexer()
        self.lexer.build(expr + "$")
        self.symb = self.lexer.token()

    # Z -> B $           { true, false, not, ( }
    def rec_Z(self):
        if self.symb in ["true", "false", "not", "("]:
            return self.rec_B() and self.rec_terminal("$")
        else:
            return False

    # B -> T B'          { true, false, not, ( }
    def rec_B(self):
        if self.symb in ["true", "false", "not", "("]:
            return self.rec_T() and self.rec_Bl()
        else:
            return False

    # B' -> or B B'      { or }
    # B' -> €            { $, ), or }
    def rec_Bl(self):
        if self.symb == "or":
            return self.rec_terminal("or") and self.rec_B() and self.rec_Bl()
        elif self.symb in ["$", ")", "or"]:
            return True
        else:
            return False

    # T -> F T'          { true, false, not, ( }
    def rec_T(self):
        if self.symb in ["true", "false", "not", "("]:
            return self.rec_F() and self.rec_Tl()
        else:
            return False

    # T' -> and F T'     { and }
    # T' -> €            { or, $, ) }
    def rec_Tl(self):
        if self.symb == "and":
            return self.rec_terminal("and") and self.rec_F() and self.rec_Tl()
        elif self.symb in ["or", "$", ")"]:
            return True
        else:
            return False

    # F -> true          { true }
    # F -> false         { false }
    # F -> not F         { not }
    # F -> ( B )         { ( }
    def rec_F(self):
        if self.symb in ["true", "false"]:
            return self.rec_terminal(self.symb)
        elif self.symb == "not":
            return self.rec_terminal("not") and self.rec_F()
        elif self.symb == "(":
            return self.rec_terminal("(") and self.rec_B() and self.rec_terminal(")")
        else:
            return False

    def rec_terminal(self, s):
        if self.symb == s:
            self.symb = self.lexer.token()
            return True
        else:
            return False


lg = LogicGrammar("true and not(false and not(false))")
if lg.rec_Z():
    print("Expressão Válida")
else:
    print("Expressão Inválida")