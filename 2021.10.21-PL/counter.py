# counter.py
import ply.lex as plex
from my_lib import slurp
tokens = ("NL", "OTHER", "UC", "LC", "DIG", "SPC")
counter = {}

def t_DIG(t):
    r"[0-9]+"
    global counter
    counter["digit"] = counter.get("digit", 0) + len(t.value)
    pass

def t_SPC(t):
    r"[ ]+"
    global counter
    counter["space"] = counter.get("space", 0) + len(t.value)
    pass

def t_UC(t):
    r"[A-Z]+"
    global counter
    counter["upper case"] = counter.get("upper case", 0) + len(t.value)
    pass

def t_LC(t):
    r"[a-z]+"
    global counter
    counter["lower case"] = counter.get("lower case", 0) + len(t.value)
    pass

def t_OTHER(t):
    r"."
    global counter
    counter["other"] = counter.get("other", 0) + 1
    pass

def t_NL(t):
    r"\n"
    global counter
    counter["new lines"] = counter.get("new lines", 0) + 1
    pass


def t_error(t):
    print("Unexpected token... really?")
    exit(1)


lexer = plex.lex()
lexer.input(slurp("Chemistry.py"))
lexer.token()
for chars in counter.keys():
    print(f"Number of {chars} characters: {counter[chars]}")

