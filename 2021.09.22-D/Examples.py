from os import getcwd


class Rectangle:
    # Constructor
    def __init__(self, length=0, height=0):
        self.length = length
        self.height = height

    # Python representation
    def __repr__(self):
        return f"Rectangle({self.length}, {self.height})"

    # Python toString
    def __str__(self):
        return f"rect({self.length} x {self.height})"

    def area(self):
        return self.length * self.height

    def scale(self, factor):
        self.length *= factor
        self.height *= factor


r = Rectangle(10, 20)
r.scale(0.5)
print(r.area())

print(getcwd())
