#!/usr/bin/env python3
from my_utils import slurp
import ply.lex as lex
import sys

tokens = ("TAGS", "TEXT")
t_TEXT = r"[^<]+"

def t_TAGS(t):
    r"<[^>]+>"
    pass        # no return value

def t_error(t):
    exit(1)


if len(sys.argv) != 2:
    print("Missing filename")
    exit(1)

contents = slurp(sys.argv[1])
lexer = lex.lex()
lexer.input(contents)
for token in iter(lexer.token, None):
    print(token.value)


