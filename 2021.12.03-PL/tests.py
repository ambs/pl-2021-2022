# tests.py
from logic_grammar import LogicGrammar
import pytest


@pytest.mark.parametrize(
    argnames=["expr", "golden_output"],
    argvalues=[
        ("true", True),
        ("false", False),
        ("batatas <- false", None),
        ("batatas", False)
    ]
)
def test_parser(expr, golden_output):
    lg = LogicGrammar()
    assert(lg.parse(expr) == golden_output)


def test_exception():
    with pytest.raises(Exception, match=r'Unknown variable'):
        lg = LogicGrammar()
        lg.parse("cebolas")

