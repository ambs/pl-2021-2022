# logic_eval

def xor(a, b):
    return (a or b) and not (a and b)


class LogicEval:
    # symbol table / Tabela de Símbolos
    symbols = {}

    # Design Pattern: Dispatch Table
    operators = {
        "or": lambda ast: LogicEval.evaluate(ast["left"]) or LogicEval.evaluate(ast["right"]),
        "not": lambda ast: not LogicEval.evaluate(ast["left"]),
        "xor": lambda ast: xor(LogicEval.evaluate(ast["left"]), LogicEval.evaluate(ast["right"])),
        "and": lambda ast: LogicEval.evaluate(ast["left"]) and LogicEval.evaluate(ast["right"]),
        "+": lambda ast: LogicEval.evaluate(ast["left"]) + LogicEval.evaluate(ast["right"]),
        "-": lambda ast: LogicEval.evaluate(ast["left"]) - LogicEval.evaluate(ast["right"]),
        "*": lambda ast: LogicEval.evaluate(ast["left"]) * LogicEval.evaluate(ast["right"]),
        "/": lambda ast: LogicEval.evaluate(ast["left"]) / LogicEval.evaluate(ast["right"]),
        "attrib": lambda ast: LogicEval._attrib(ast),
    }

    @staticmethod
    def _attrib(ast):
        value = LogicEval.evaluate(ast["right"])
        LogicEval.symbols[ast["left"]] = value
        # print(LogicEval.symbols)
        return None

    @staticmethod
    def evaluate(ast):
        if type(ast) in [bool, float]:
            return ast
        if type(ast) is dict:
            return LogicEval._eval_operator(ast)
        if type(ast) is str:
            var_name = ast
            if var_name in LogicEval.symbols:
                return LogicEval.symbols[var_name]
            raise Exception(f"Undefined variable {var_name}")
        raise Exception("Unknown AST type")

    @staticmethod
    def _eval_operator(ast):
        op = ast["op"]
        if op in LogicEval.operators:
            func = LogicEval.operators[op]
            return func(ast)
        else:
            raise Exception(f"Unknown operator {op}")

