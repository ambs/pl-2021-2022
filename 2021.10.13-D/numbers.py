# numbers.py

q0 = "A"
F = {"C", "E"}   # set(["C", "E"])
Q = {"A", "B", "C", "D", "E"}
V = {"+", "-", "."}.union({str(x) for x in range(0, 10)})

TT = {}
# Fill whole TT with ERROR
for q in Q:
    TT[q] = {}
    for v in V:
        TT[q][v] = "ERROR"

# Fill digit transitions
for v in [str(x) for x in range(0, 10)]:
    for q in ["A", "B", "C"]:
        TT[q][v] = "C"
    for q in ["D", "E"]:
        TT[q][v] = "E"

# Fill exceptions
TT["A"]["+"] = "B"
TT["A"]["-"] = "B"
TT["C"]["."] = "D"


def read_word(tt, string):
    alfa = q0
    while len(string) > 0 and alfa != "ERROR":
        first_char = string[0]
        if first_char in tt[alfa]:
            alfa = tt[alfa][first_char]
        else:
            alfa = "ERROR"
        string = string[1:]

    return alfa in F and string == ""


for word in ["42", "3.1415926", "+49.3", "-42", "+3.", "batatas"]:
    r = read_word(TT, word)
    print(f"Read({word}) = {r}")

