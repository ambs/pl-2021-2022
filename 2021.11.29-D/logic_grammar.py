# logic_grammar.py
from logic_lexer import LogicLexer
import ply.yacc as pyacc


class LogicGrammar:

    precedence = (
        ("left", "and"),
        ("left", "or", "xor"),
    )

    def __init__(self):
        self.yacc = None
        self.lexer = None
        self.tokens = None

    def build(self, **kwargs):
        self.lexer = LogicLexer()
        self.lexer.build(**kwargs)
        self.tokens = self.lexer.tokens
        self.yacc = pyacc.yacc(module=self, **kwargs)

    def parse(self, string):
        self.lexer.input(string)
        return self.yacc.parse(lexer=self.lexer.lexer)

    def p_b1(self, p):
        """ B : F """
        p[0] = p[1]

    def p_b2(self, p):
        """ B : B or B """
        p[0] = dict(op="or", left=p[1], right=p[3])

    def p_b3(self, p):
        """ B : B and B """
        p[0] = {"op": "and", "left": p[1], "right": p[3]}

    def p_b4(self, p):
        """ B : B xor B """
        p[0] = {"op": "xor", "left": p[1], "right": p[3]}

    def p_f1(self, p):
        """ F : true """
        p[0] = True

    def p_f2(self, p):
        """ F : false """
        p[0] = False

    def p_f3(self, p):
        """ F : not F """
        #   0    1  2
        p[0] = {"op": "not", "left": p[2]}

    def p_f4(self, p):
        """ F : '(' B ')' """
        #   ^    ^  ^  ^
        #   0    1  2  3
        p[0] = p[2]

    def p_error(self, p):
        if p:
            print(f"Syntax error: unexpected '{p.type}'")
        else:
            print("Syntax error: unexpected end of file")
        exit(1)
