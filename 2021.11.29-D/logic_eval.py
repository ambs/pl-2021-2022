# logic_eval

def xor(a, b):
    return (a or b) and not (a and b)


class LogicEval:

    # Design Pattern: Dispatch Table
    operators = {
        "or": lambda ast: LogicEval.evaluate(ast["left"]) or LogicEval.evaluate(ast["right"]),
        "not": lambda ast: not LogicEval.evaluate(ast["left"]),
        "xor": lambda ast: xor(LogicEval.evaluate(ast["left"]), LogicEval.evaluate(ast["right"])),
        "and": lambda ast: LogicEval.evaluate(ast["left"]) and LogicEval.evaluate(ast["right"]),
    }

    @staticmethod
    def evaluate(ast):
        if type(ast) is bool:
            return ast
        if type(ast) is dict:
            return LogicEval._eval_operator(ast)
        raise Exception("Unknown AST type")

    @staticmethod
    def _eval_operator(ast):
        op = ast["op"]
        if op in LogicEval.operators:
            func = LogicEval.operators[op]
            return func(ast)
        else:
            raise Exception(f"Unknown operator {op}")

