# logic.py
from logic_grammar import LogicGrammar
from logic_eval import LogicEval
from pprint import PrettyPrinter

pp = PrettyPrinter()

lg = LogicGrammar()
lg.build()

for expr in iter(lambda: input(">> "), ""):
    ans = lg.parse(expr)
    pp.pprint(ans)
    print(f"<< {LogicEval.evaluate(ans)}")


