# logic.py
from logic_grammar import LogicGrammar

lg = LogicGrammar()
lg.build()
ans = lg.parse("not true and not(false and not(false))")

print(f"Result: {ans}")
