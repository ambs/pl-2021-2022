# playlist.py
import ply.lex as plex
from my_utils import slurp
from pprint import PrettyPrinter


class Playlist:
    tokens = ("AUTHOR", "TITLE", "TIME", "NOTES")
    states = (("titulo", "exclusive"),
              ("tempo", "exclusive"),
              ("comentario", "exclusive"))

    t_ANY_ignore = "\t"
    t_ignore = ""

    def t_ANY_error(self, t):
        print(f"Unexpected token {t.value[:11]}")
        exit(1)

    def t_STR(self, t):
        r"[^\t]+"
        t.type = "AUTHOR"
        t.lexer.begin("titulo")
        self.author = t.value
        pass

    def t_NOTES(self, t):
        r"\t.*\n"
        pass

    def t_titulo_STR(self, t):
        r"[^\t]+"
        t.lexer.begin("tempo")
        t.type = "TITLE"
        pass

    def t_tempo_TIME(self, t):
        r"[0-9]+:[0-5][0-9]"
        t.lexer.begin("comentario")

        (minutos, _) = t.value.split(":")
        if int(minutos) >= 3:
            self.counts[self.author] = self.counts.get(self.author, 0) + 1
        # if self.author not in self.counts:
        #     self.counts[self.author] = 0
        # self.counts[self.author] += 1
        pass

    def t_comentario_NOTES(self, t):
        r".*\n"
        t.lexer.begin("INITIAL")
        pass

    def __init__(self, filename):
        self.lexer = None
        self.filename = filename
        self.counts = {}  # author => nr musics with more than 3 min
        self.author = None  # last author read

    def parse(self, **kwargs):
        self.lexer = plex.lex(module=self, **kwargs)
        self.lexer.input(slurp(self.filename))
        for _ in iter(self.lexer.token, None):
            pass
        pp = PrettyPrinter()
        pp.pprint(self.counts)


playlist = Playlist("Playlist.txt")
playlist.parse()














