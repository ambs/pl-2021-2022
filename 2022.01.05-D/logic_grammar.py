# logic_grammar.py
from logic_lexer import LogicLexer
import ply.yacc as pyacc


class LogicGrammar:

    precedence = (
        ("left", "or", "xor"),
        ("left", "and"),
        ("left", "+", "-"),
        ("left", "*", "/")
    )

    def __init__(self):
        self.yacc = None
        self.lexer = None
        self.tokens = None

    def build(self, **kwargs):
        self.lexer = LogicLexer()
        self.lexer.build(**kwargs)
        self.tokens = self.lexer.tokens
        self.yacc = pyacc.yacc(module=self, **kwargs)

    def parse(self, string):
        self.lexer.input(string)
        return self.yacc.parse(lexer=self.lexer.lexer)

    def p_code0(self, p):  # our axiom!
        """ code : S """
        #    ^     ^
        #  LISTA  AST
        p[0] = [p[1]]

    def p_code1(self, p):
        """ code : code ';' S """
        #    ^      ^       ^
        #  LISTA   LISTA   AST
        p[0] = p[1]
        p[0].append(p[3])

    def p_s0(self, p):
        """S : A
             | E
             | C"""
        p[0] = p[1]
    
    def p_C0(self, p):
        """ C : print e_list """
        p[0] = {'op': p[1], 'args': p[2]}

    def p_C1(self, p):
        """ C : for var '[' E ',' E ']' '{' code '}' """
        p[0] = {'op': 'for', 'args': [{'var': p[2]}, p[4], p[6]], 'code': p[9]}

    def p_elist0(self, p):
        """ e_list : E
                   | string """
        p[0] = [p[1]]

    def p_elist1(self, p):
        """ e_list : e_list ',' E
                   | e_list ',' string """
        p[0] = p[1]
        p[0].append(p[3])

    def p_n1(self, p):
        """N : nr """
        p[0] = p[1]

    def p_n2(self, p):
        """N : E '+' E
             | E '-' E
             | E '*' E
             | E '/' E """
        p[0] = {"op": p[2], "args": [p[1], p[3]]}

    def p_b1(self, p):
        """ B : F """
        p[0] = p[1]

    def p_b2(self, p):
        """ B : E or E
              | E and E
              | E xor E """
        p[0] = {"op": p[2], "args": [p[1], p[3]]}

    def p_f1(self, p):
        """ F : true """
        p[0] = True

    def p_f2(self, p):
        """ F : false """
        p[0] = False

    def p_f3(self, p):
        """ F : not F 
              | not var"""
        p[0] = {"op": "not", "args": [p[2]]}

    def p_a1(self, p):
        """ A : var '=' E """
        p[0] = {'op': 'attrib', 'args': [{'var': p[1]}, p[3]]}

    def p_a2(self, p):
        """ A : fun var '(' args ')' '{' code '}' """
        p[0] = {'op': 'fun', 'args': [{'var': p[2]}, p[4]], "code": p[7]}

    def p_args(self, p):
        """ args :
                 | var_list """
        if len(p) == 1:  # regra de cima
            p[0] = []
        else:            # segunda regra
            p[0] = p[1]

    def p_var_list(self, p):
        """ var_list : var
                     | var_list ',' var """
        if len(p) == 2:
            p[0] = [{'var': p[1]}]
        else:
            p[0] = p[1]
            p[0].append({'var': p[3]})

    def p_E0(self, p):
        """E : B
             | N
             | var
             | '(' E ')' """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[2]

    def p_E1(self, p):
        """ E : var '(' arg_list ')'
              | var '(' ')' """
        arg_list = [] if p[3] == ')' else p[3]
        p[0] = {"op": "call", "args": [{"var": p[1]}, {"argList": arg_list}]}

    def p_arg_list(self, p):
        """ arg_list : E
                     | arg_list ',' E """
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1]
            p[0].append(p[3])

    def p_error(self, p):
        if p:
            raise Exception(f"Syntax error: unexpected '{p.type}'")
        else:
            raise Exception("Syntax error: unexpected end of file")
