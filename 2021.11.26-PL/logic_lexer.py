# logic_lexer.py
import ply.lex as plex


class LogicLexer:
    keywords = ("true", "false", "not", "and", "or")
    tokens = keywords + ("stuff",)
    literals = [")", "(", "\n"]
    t_ignore = " \t"

    def t_keywords(self, t):
        r"""[a-z]+"""
        t.type = t.value if t.value in self.keywords else "stuff"
        return t

    def t_error(self, t):
        raise Exception(f"Unexpected token {t.value[:10]}")

    def __init__(self):
        self.lex = plex.lex(module=self)

    def token(self):
        return self.lex.token()







