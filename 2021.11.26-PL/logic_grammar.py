# logic_grammar.py
from logic_lexer import LogicLexer
import ply.yacc as pyacc


class LogicGrammar:
    precedence = (
        ("left", "or"),
    )

    def p_error(self, p):
        if p:
            raise Exception(f"Parse Error: Unexpected token '{p.type}'")
        else:
            raise Exception("Parse Error: Expecting token")

    def p_b1(self, p):
        """ b : t """
        p[0] = p[1]

    def p_b2(self, p):
        """ b : b or b """
        p[0] = p[1] or p[3]

    def p_t1(self, p):
        """ t : f """
        p[0] = p[1]

    def p_t2(self, p):
        """ t : t and f """
        p[0] = p[1] and p[3]

    def p_f1(self, p):
        """ f : true """
        p[0] = True

    def p_f2(self, p):
        """ f : false """
        p[0] = False

    def p_f3(self, p):
        """ f : not f """
        p[0] = not p[2]

    def p_f4(self, p):
        """ f : '(' b ')' """
        #   ^    ^  ^  ^
        #   0    1  2  3
        p[0] = p[2]

    def __init__(self):
        self.lexer = LogicLexer()
        self.tokens = self.lexer.tokens
        self.yacc = pyacc.yacc(module=self)

    def parse(self, expression):
        return self.yacc.parse(lexer=self.lexer.lex, input=expression)




