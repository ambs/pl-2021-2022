# main.py

from calclexer import CalcLexer

calculator = CalcLexer.builder()

for formula in iter(lambda: input(">> "), ""):
    try:
        answer = calculator.calculate(formula)
        print(f"= {answer}")
    except Exception as e:
        print(e)

