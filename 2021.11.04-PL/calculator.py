#!/usr/bin/env python3
# author: Rui Alves @ IPCA 15505

import numbers
from operator import truediv, mul, add, sub

# Define operators
operators = {
    'ADD': add,
    'SUB': sub,
    'MUL': mul,
    'DIV': truediv
}


class Calculator:
    """A stack based calculator also known as Reverse Polish Notation"""

    def __init__(self):
        """base constructor"""
        self.stack = []

    def push(self, value):
        """Function to push values to stack"""
        if isinstance(value, numbers.Number):
            self.stack.append(value)
        else:
            raise TypeError(f"\"{value}\" should be a number")

    def calculate(self, operator):
        """Function to calculate operators"""
        if len(self.stack) >= 2:
            if operator == "DIV" and self.top() == 0:
                raise ZeroDivisionError("Can't divide by 0")

            operator_a = self.pop()
            operator_b = self.pop()
            self.push(operators.get(operator)(operator_b, operator_a))

    def top(self):
        """"Function to return top value"""
        # check if list is empty
        if not self.stack:
            raise Exception('Stack must not be empty')
        return self.stack[-1]

    def pop(self):
        """Function to remove and return value in stack"""
        # check if list is empty
        if not self.stack:
            raise Exception('Stack must not be empty')
        return self.stack.pop()
