# calclexer.py
import ply.lex as plex
from calculator import Calculator


class CalcLexer:
    tokens = ("OP", "NUMBERS")
    t_ignore = " "

    def t_OP(self, t):
        r"""ADD|MUL|SUB|DIV"""
        self.calc.calculate(t.value)
        pass

    def t_NUMBERS(self, t):
        r"""\-?[0-9]+(\.[0-9]+)?"""
        self.calc.push(float(t.value))
        pass

    def t_error(self, t):
        raise Exception(f"Unexpected token error: {t.value[:10]}")

    def __init__(self):
        self.lexer = None
        self.calc = None

    def initialize(self, **kwargs):
        self.lexer = plex.lex(module=self, **kwargs)

    def calculate(self, formula):
        self.lexer.input(formula)
        self.calc = Calculator()
        self.lexer.token()
        return self.calc.top()

    @staticmethod
    def builder(**kwargs):
        obj = CalcLexer()
        obj.initialize(**kwargs)
        return obj
