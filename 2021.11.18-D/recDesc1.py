# recDesc1.py

class RecDescent:

    def __init__(self):
        self.pos = 0
        self.string = ""

    def parse(self, frase):
        self.pos = 0
        self.string = frase + "a"
        return self.rec_Z()

    def symbol(self):
        return self.string[self.pos]

    # Z -> S a          L = { z }
    def rec_Z(self):
        if self.symbol() == 'z':  # simbolo a reconhecer é o lookahead?
            return self.rec_S() and self.rec_terminal('a')
        return False

    # S -> z P          L = { z }
    def rec_S(self):
        if self.symbol() == 'z':
            return self.rec_terminal('z') and self.rec_P()
        return False

    # P -> X | Y        L = { x } | L = { y, a }
    def rec_P(self):
        if self.symbol() == 'x':
            return self.rec_X()
        if self.symbol() in ['y', 'a']:
            return self.rec_Y()
        return False

    # X -> x Q          L = { x }
    def rec_X(self):
        if self.symbol() == 'x':
            return self.rec_terminal('x') and self.rec_Q()
        return False

    # Q -> x Q | €      L = { x } | L = { a }
    def rec_Q(self):
        if self.symbol() == 'x':
            return self.rec_terminal('x') and self.rec_Q()
        if self.symbol() == 'a':
            return True
        return False

    # Y -> y Y | €      L = { y } | L = { a }
    def rec_Y(self):
        if self.symbol() == 'y':
            return self.rec_terminal('y') and self.rec_Y()
        if self.symbol() == 'a':
            return True
        return False

    def rec_terminal(self, symbol):
        if symbol == self.symbol():
            self.pos += 1
            return True
        return False


parser = RecDescent()
for texto in iter(lambda: input("Frase: "), ""):
    print(parser.parse(texto))

