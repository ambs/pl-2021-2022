# logic_grammar.py
import ply.yacc as pyacc
from pprint import PrettyPrinter

from logic_lexer import LogicLexer
from logic_eval import LogicEval


class LogicGrammar:
    precedence = (
        ("left", "+", "-"),
        ("left", "*", "/"),
        ("right", "uminus"),
        ("left", "or", "xor"),   # menor prioridade
        ("left", "and"),
    )

    def p_error(self, p):
        if p:
            raise Exception(f"Parse Error: Unexpected token '{p.type}'")
        else:
            raise Exception("Parse Error: Expecting token")

    #

    def p_s1(self, p):
        """ s : e """
        p[0] = p[1]

    def p_s2(self, p):
        """ s : var assign e """
        p[0] = {"op": "assign", "args": [p[1], p[3]]}

    #

    def p_n1(self, p):
        """ n : nr
              | '-' e  %prec uminus  """
        p[0] = p[1] if len(p) == 2 else {"op": "-", "args": [0.0, p[2]]}

    def p_n2(self, p):
        """ n : e '+' e
              | e '-' e
              | e '*' e
              | e '/' e """
        p[0] = dict(op=p[2], args=[p[1], p[3]])

    #

    def p_b1(self, p):
        """ b : f
              | e or e
              | e and e
              | e xor e """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = dict(op=p[2], args=[p[1], p[3]])

    #

    def p_f1(self, p):
        """ f : true """
        p[0] = True

    def p_f2(self, p):
        """ f : false """
        p[0] = False

    def p_f3(self, p):
        """ f : not f """
        p[0] = dict(op="not", args=[p[2]])

    #

    def p_e1(self, p):
        """ e : var """
        p[0] = {'var': p[1]}

    def p_e2(self, p):
        """ e : '(' e ')' """
        p[0] = p[2]

    def p_e3(self, p):
        """ e : b
              | n """
        p[0] = p[1]

    def __init__(self):
        self.lexer = LogicLexer()
        self.tokens = self.lexer.tokens
        self.yacc = pyacc.yacc(module=self)

    def parse(self, expression):
        ans = self.yacc.parse(lexer=self.lexer.lex, input=expression)
        pp = PrettyPrinter()
        pp.pprint(ans)  # debug rulez!
        return LogicEval.eval(ans)




