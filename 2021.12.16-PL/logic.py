# logic.py
from logic_grammar import LogicGrammar

lg = LogicGrammar()
for e in iter(lambda: input(">> "), ""):
    try:
        ans = lg.parse(e)
        if ans is not None:
            print(f"<< {ans}", end="\n\n")
    except Exception as exception:
        print(exception)


