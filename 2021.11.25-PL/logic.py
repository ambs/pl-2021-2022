# logic.py

class LogicGrammar:
    def symb(self):
        return self.expression[self.symb_index]

    # Z -> B \n    Lookaheads = { true, false, not, ( }
    def rec_z(self):
        if self.symb() in ["true", "false", "not", "("]:
            return self.rec_b() and self.rec_terminal("\n")
        else:
            return False

    # B -> T B'   Lookaheads = { true, false, not, ( }
    def rec_b(self):
        if self.symb() in ["true", "false", "not", "("]:
            return self.rec_t() and self.rec_bl()
        else:
            return False

    # B' -> € | or T B'  Lookaheads = {\n, )} | { or }
    def rec_bl(self):
        if self.symb() == "or":
            return self.rec_terminal("or") and self.rec_t() and self.rec_bl()
        elif self.symb() in ["\n", ")"]:
            return True
        else:
            return False

    # T -> F T'   Lookaheads = {true, not, false, (}
    def rec_t(self):
        if self.symb() in ["true", "false", "not", "("]:
            return self.rec_f() and self.rec_tl()
        else:
            return False

    # T' -> € | and F T'  Lookaheads={or,),\n} | {and}
    def rec_tl(self):
        if self.symb() == "and":
            return self.rec_terminal("and") and self.rec_f() and self.rec_tl()
        elif self.symb() in ["or", ")", "\n"]:
            return True
        else:
            return False

    # F -> true | false | not F | ( B )  Ls={true}|{false}|{not}|{(}
    def rec_f(self):
        if self.symb() in ["true", "false"]:
            return self.rec_terminal(self.symb())
        elif self.symb() == "not":
            return self.rec_terminal("not") and self.rec_f()
        elif self.symb() == "(":
            return self.rec_terminal("(") and self.rec_b() and self.rec_terminal(")")
        else:
            return False

    def rec_terminal(self, t):
        if self.symb() == t:
            self.symb_index += 1
            return True
        else:
            return False

    def __init__(self, expression):
        self.expression = expression.split(" ")
        self.expression.append("\n")
        self.symb_index = 0


for e in iter(lambda: input(">> "), ""):
    lg = LogicGrammar(e)
    print(lg.rec_z())
